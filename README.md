CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Maintainers


INTRODUCTION
------------

The Devel Snippet module allows to save code snippets inputted on the
Execute PHP Code page (provided by the
[Devel](https://drupal.org/project/devel) module) to database for
further execution.

  * For a full description of the module, visit the project page:  
    https://drupal.org/project/devel_snippet

  * To submit bug reports and feature suggestions, or to track changes:  
    https://drupal.org/project/issues/devel_snippet


REQUIREMENTS
------------

This module requires the following modules:

  * Devel (https://drupal.org/project/devel)


INSTALLATION
------------

  * Install as you would normally install a contributed Drupal module. See:  
    https://www.drupal.org/documentation/install/modules-themes/modules-8  
    for further information.


CONFIGURATION
-------------

  * Configure user permissions in Administration » People » Permissions:

    * Administer Devel Snippet entities (Devel Snippet module)  
      The only permission provided by the module and required for
      accessing module's pages and perform actions with code snippets.  
      **Give to trusted roles only.**


MAINTAINERS
-----------

Current maintainers:

  * Sergey Sergin (kruhak) - https://www.drupal.org/u/kruhak
  * Anton Shubkin (antongp) - https://www.drupal.org/u/antongp


This project is created by [ADCI Solutions](http://drupal.org/node/1542952) team.
